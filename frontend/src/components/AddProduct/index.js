import React, { Component } from 'react';
import { compose } from 'recompose';

import { withFirebase } from '../Firebase';
import './index.css';

const AddProductPage = () => (
  <div>
    <AddProductForm />
  </div>
);

const INITIAL_STATE = {
  prod_name: '',
  prod_price: 0,
};

class AddProductFormBase extends Component {
  constructor(props) {
    super(props);
    const { uid } = props.firebase.auth.currentUser;

    this.state = {
      ...INITIAL_STATE,
      uid
    };
  }

  onSubmit = event => {

    const key = Math.random().toString(36).substring(2, 15);
    const { prod_name, prod_price, uid } = this.state;

    this.props.firebase
      .product(key)
      .set({
        prod_name,
        prod_price,
        uid,
      })
      .then(authUser => {
        this.setState({ ...INITIAL_STATE });
      })
      .catch(error => {
        this.setState({ error });
      });


    event.preventDefault();
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {

    const {
      prod_name,
      prod_price,
    } = this.state;

    const isInvalid =
      prod_name === '' ||
      prod_price === 0;

    return (
      <div class="spacing">
        <form class="form_content" onSubmit={this.onSubmit}>
          <h1 class="title t-spacing">Add Product</h1>
          <div class="form-group">
          <input
              name="prod_name"
              value={prod_name}
              onChange={this.onChange}
              type="text"
              placeholder="Product Name"
              class="form-control"
            />
          </div>
          <div class="form-group">
            <label>Price (€)</label>
            <input
                name="prod_price"
                value={prod_price}
                onChange={this.onChange}
                type="number"
                placeholder="Price"
                class="form-control"
              />
          </div>
            <button class="form_btn" disabled={isInvalid} type="submit">Add Product</button>
        </form>
      </div>
    );
  }
}


const AddProductForm = compose(
  withFirebase,
)(AddProductFormBase);

export default AddProductPage;

export { AddProductForm };
