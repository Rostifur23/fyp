import React from 'react';
import { withFirebase } from '../Firebase';

import './index.css';

const driver = require('bigchaindb-driver');

const API_PATH = 'https://test.bigchaindb.com/api/v1/';
const api = `${API_PATH}transactions/`;
const conn = new driver.Connection(API_PATH);

class ViewTransactions extends React.Component {
	constructor(props) {
		super(props)
		const { uid } = props.firebase.auth.currentUser;

		this.pubKeyRef = this.props.firebase.user(uid).child('publicK');
		
		this.state = {
			publicKey: '',
			lastRequestedTransaction: 0,
			transactions: [],
			transactionKeys: []
		};
	}

	getTransactionData = key => {
		return fetch(`${api}${key}`)
			.then(response => response.json())
	}

	requestNextPage = async () => {
		const { transactions, transactionKeys, lastRequestedTransaction: prevLastRequestedTransaction } = this.state;
		if (transactionKeys.length === prevLastRequestedTransaction) { return }
		
		const lastRequestedTransaction = Math.min(transactions.length + 10, transactionKeys.length);

		this.setState({lastRequestedTransaction});

		const nextPageOfTransactionRequests = [...transactionKeys]
			.slice(
				transactions.length,
				lastRequestedTransaction
			)
			.map(key => this.getTransactionData(key));

		

		const nextPageOfTransactionValues = await Promise.all(nextPageOfTransactionRequests);
		
		this.setState(prevState => ({
			transactions: [
				...prevState.transactions,
				...nextPageOfTransactionValues
			]
		}));
	}

	getTransactionList = publicKey => {
		conn
			.listOutputs(publicKey)
			.then(data => {
				this.setState({
					transactionKeys: data.map(transactionKeyObject => transactionKeyObject.transaction_id)
				});
				
				this.requestNextPage();
			});
	}

	componentDidMount() {
    this.pubKeyRef.on('value', snap => {
			const publicKey = snap.val();
			this.setState({ publicKey });
			
			if (!this.state.transactions.length) {
				this.getTransactionList(publicKey);
			}
    });
	}

	componentWillMount() {
    this.pubKeyRef.off('value');
  }

  render() {
		const { transactions, transactionKeys, lastRequestedTransaction } = this.state;
		console.log(transactions);

  	return( 
  		<div className="transactions">
				<table class="trans_table">
					<thead>
						<tr>
							<th>Product Code</th>
							<th>Product Amount</th>
							<th>Sent From</th>
							<th>Sent To</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						{transactions.map(({id, asset}) => {
							const { prod_code, prod_amt, vendor_from, vendor_to, datetime } = asset.data;
							return <tr key={id}>
								<td>{prod_code}</td>
								<td>{prod_amt}</td>
								<td>{vendor_from}</td>
								<td>{vendor_to}</td>
								<td>{datetime}</td>
							</tr>
						})}
						{(!transactions.length) &&
							<tr>
								<td colSpan='5'>No transactions found</td>
							</tr>
        		}
					</tbody>
				</table>
				{transactionKeys.length !== lastRequestedTransaction &&
					<div class="btn_wrapper">
						<button class="load_btn" onClick={this.requestNextPage}>Load more</button>
					</div>
				}
  		</div>
		)
  }
}

export default withFirebase(ViewTransactions);
