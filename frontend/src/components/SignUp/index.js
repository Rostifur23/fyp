import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import './index.css';
import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';

const driver = require('bigchaindb-driver');
const userKeys = new driver.Ed25519Keypair();
const privateK = userKeys.privateKey;
const publicK = userKeys.publicKey;

const SignUpPage = () => (
  <div>
    <h1 class="title">SignUp</h1>
    <span class="center">
      <SignUpForm />
      <SignInLink />
    </span>
  </div>
);

const INITIAL_STATE = {
  company_name: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class SignUpFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { company_name, email, passwordOne } = this.state;

    this.props.firebase
    .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // Create a user in your Firebase realtime database
        return this.props.firebase
          .user(authUser.user.uid)
          .set({
            company_name,
            email,
            publicK,
            privateK,
          });
      })
      .then(authUser => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {

    const {
      company_name,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '' ||
      company_name === '';

    return (
      <form class="form_content" onSubmit={this.onSubmit}>
        <div class="form-group">
          <input
                name="company_name"
                value={company_name}
                onChange={this.onChange}
                type="text"
                placeholder="Company Name"
                class="form-control"
          />
        </div>
        <div class="form-group">
          <input
                name="email"
                value={email}
                onChange={this.onChange}
                type="text"
                placeholder="Email Address"
                class="form-control"
          />
        </div>
        <div class="form-group">
          <input
                name="passwordOne"
                value={passwordOne}
                onChange={this.onChange}
                type="password"
                placeholder="Password"
                class="form-control"
          />
        </div>
        <div class="form-group">
          <input
                name="passwordTwo"
                value={passwordTwo}
                onChange={this.onChange}
                type="password"
                placeholder="Confirm Password"
                class="form-control"
          />
        </div>
          
            
            
            

        <button class="form_btn" disabled={isInvalid} type="submit">Sign Up</button>

        {error && <p>{error.message}</p>}
      </form>
    );
  }
}

const SignUpLink = () => (
  <p>
    Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);

const SignInLink = () => (
  <p>
    Already have an account? <Link to={ROUTES.SIGN_IN}>Sign In</Link>
  </p>
);

const SignUpForm = compose(
  withRouter,
  withFirebase,
)(SignUpFormBase);

export default SignUpPage;

export { SignUpForm, SignUpLink };
