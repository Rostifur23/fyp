import React, {Component} from 'react';
import MapGL, {NavigationControl} from 'react-map-gl';
const TOKEN = process.env.REACT_APP_MAP_KEY;

const navStyle = {
  position: 'absolute',
  top: 0,
  right: 0,
  padding: '10px'
};

export default class Map extends Component {
  constructor(props) {
      super(props);
      this.state = {
        viewport: {
          latitude: 53.3498,
          longitude: -6.2603,
          bearing: 0,
          pitch: 0,
          zoom: 3,
          width: 100,
          height: 500,
        }
      };
    }

render() {
  const {viewport} = this.state;
    return (
      <MapGL
        {...viewport}
        onViewportChange={(viewport) => this.setState({viewport})}
        mapStyle="mapbox://styles/mapbox/dark-v9"
        mapboxApiAccessToken={TOKEN}
        width='100%'
        >
        <div className="nav" style={navStyle}>
          <NavigationControl onViewportChange={(viewport) => this.setState({viewport})} />
        </div>
      </MapGL>
    );
  }
}
