import React, { Component } from 'react';
import { compose } from 'recompose';

import { withFirebase } from '../Firebase';
import './index.css';

const AddVendorPage = () => (
  <div>
    <AddVendorForm />
  </div>
);

const INITIAL_STATE = {
  vend_name: '',
  vend_type: "Supplier",
};

class AddVendorFormBase extends Component {
  constructor(props) {
    super(props);
    const { uid } = props.firebase.auth.currentUser;

    this.state = {
      ...INITIAL_STATE,
      uid
     };
  }

  onSubmit = event => {

    const key = Math.random().toString(36).substring(2, 15);
    const { vend_name, vend_type, uid } = this.state;

    this.props.firebase
      .vendor(key)
      .set({
        vend_name,
        vend_type,
        uid,
      })
      .then(authUser => {
        this.setState({ ...INITIAL_STATE });
      })
      .catch(error => {
        this.setState({ error });
      });


    event.preventDefault();
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {

    const {
      vend_name,
    } = this.state;

    const isInvalid =
      vend_name === '';

    return (
      <div class="spacing">
        <form class="form_content" onSubmit={this.onSubmit}>
          <h1 class="title t-spacing">Add Vendor</h1>
          <div class="form-group">
            <input
                name="vend_name"
                value={vend_name}
                onChange={this.onChange}
                type="text"
                placeholder="Vendor Name"
                class="form-control"
              />
          </div>
          <div class="form-group">
            <select
                name="vend_type"
                onChange={this.onChange}
                class="form-control"
              >
                <option value="Supplier">Supplier</option>
                <option value="Store">Store</option>
                <option value="Distribution Center">Distribution Center</option>
              </select>
          </div>
          <button class="form_btn" disabled={isInvalid} type="submit">Add Vendor</button>
        </form>
      </div>
    );
  }
}


const AddVendorForm = compose(
  withFirebase,
)(AddVendorFormBase);

export default AddVendorPage;

export { AddVendorForm };
