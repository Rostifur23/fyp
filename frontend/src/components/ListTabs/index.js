import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";
import VendorList from '../Vendors';
import ProductList from '../Products';




class ListTabs extends React.Component {
    render() {
      return (
        <Tabs>
            <TabList>
                <Tab>View Products</Tab>
                <Tab>View Vendors</Tab>
            </TabList>

            <TabPanel>
                <ProductList />
            </TabPanel>
            <TabPanel>
                <VendorList />
            </TabPanel>
        </Tabs>
      );
    }
  }
  
export default ListTabs;