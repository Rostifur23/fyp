import React from 'react';
import { Link } from 'react-router-dom';

import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import './index.css';
import { AuthUserContext } from '../Session';

const Navigation = () => (
  <div>
    <AuthUserContext.Consumer>
      {authUser =>
        authUser ? <NavigationAuth /> : <NavigationNonAuth />
      }
    </AuthUserContext.Consumer>
  </div>
);

const NavigationAuth = () => (
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <Link class="navbar-brand" to={ROUTES.HOME}>MySupply</Link>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <Link class="nav-item nav-link" to={ROUTES.HOME}>Home</Link>
      <Link class="nav-item nav-link"  to={ROUTES.ACCOUNT}>Account</Link>
      <SignOutButton />
    </div>
  </div>
</nav>
);

const NavigationNonAuth = () => (
  <ul>
  </ul>
);

export default Navigation;
