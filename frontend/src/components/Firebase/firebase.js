import React from 'react';
import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import { Redirect } from 'react-router-dom';
import { SIGN_IN } from '../../constants/routes';

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
};

class Firebase {
  constructor() {
    app.initializeApp(config);

    this.auth = app.auth();
    this.db = app.database();
  }

  // REGISTER
  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  // LOGIN
  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  // LOGOUT
  doSignOut = () => this.auth.signOut();

  // PASSWORD RESET
  doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

  // PASSWORD UPDATE
  doPasswordUpdate = password =>
    this.auth.currentUser.updatePassword(password);

  user = uid => this.db.ref(`users/${uid}`);

  users = () => this.db.ref('users');

  product = pid => this.db.ref(`products/${pid}`);

  products = () => this.db.ref('products');

  vendor = vid => this.db.ref(`vendors/${vid}`);

  vendors = () => this.db.ref('vendors');

  myVendors = () => {
    const { currentUser } = this.auth;
    if (currentUser) {
      const { uid } = currentUser;
      
      return this.db
        .ref('vendors')
        .orderByChild('uid')
        .equalTo(uid);
    } else {
      return <Redirect to={SIGN_IN} />;
    }
  };

  myProducts = () => {
    const { currentUser } = this.auth;
    if (currentUser) {
      const { uid } = currentUser;
      
      return this.db
        .ref('products')
        .orderByChild('uid')
        .equalTo(uid);
    } else {
      return <Redirect to={SIGN_IN} />;
    }
  };

}

export default Firebase;
