import React, { Component } from 'react';
import { compose } from 'recompose';


import { withFirebase } from '../Firebase';
import './index.css';

const driver = require('bigchaindb-driver');

const API_PATH = 'https://test.bigchaindb.com/api/v1/';
const conn = new driver.Connection(API_PATH);

const INITIAL_STATE = {
  prod_code: '',
  prod_amt: 0,
  vend_to: '',
  vend_from: '',
};

const LogTransactionPage = () => (
  <div>
    <LogTransactionForm />
  </div>
);

class LogTransactionFormBase extends Component {
  constructor(props) {
    super(props);
    const { uid } = props.firebase.auth.currentUser;

    this.priKeyRef = this.props.firebase.user(uid).child('privateK');
    this.pubKeyRef = this.props.firebase.user(uid).child('publicK');

    this.state = {
      ...INITIAL_STATE,
      uid,
      privateKey: '',
      publicKey: '',

    };
  }

  componentDidMount() {
    this.priKeyRef.on('value', snap => {
      this.setState({ privateKey: snap.val() });
    });

    this.pubKeyRef.on('value', snap => {
      this.setState({ publicKey: snap.val() });
    });
  }

  componentWillMount() {
    this.priKeyRef.off('value');
    this.pubKeyRef.off('value');
  }

  onSubmit = event => {
    const { prod_code, prod_amt, vend_to, vend_from, publicKey, privateKey, uid } = this.state;

    // BLOCKCHAIN LOG HERE
    const tx = driver.Transaction.makeCreateTransaction(
      // Define the asset to store, in this example it is the current temperature
      // (in Celsius) for the city of Berlin.
      {
        prod_code: prod_code,
        prod_amt: prod_amt,
        vendor_from: vend_from,
        vendor_to: vend_to,
        uid: uid,
        datetime: new Date().toString()
      },
  
      // Metadata contains information about the transaction itself
      // (can be `null` if not needed)
      { what: 'An asset tracking transaction for MySupply' },
  
      // A transaction needs an output
      [ driver.Transaction.makeOutput(
              driver.Transaction.makeEd25519Condition(publicKey))
      ],
      publicKey
    );

    const txSigned = driver.Transaction.signTransaction(tx, privateKey);

    conn.postTransactionCommit(txSigned)
      .then(retrievedTx => console.log('Transaction', retrievedTx.id, 'successfully posted.'));
      

    this.setState({ ...INITIAL_STATE });
    event.preventDefault();
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {

    const {
      prod_code,
      prod_amt,
      vend_to,
      vend_from,
    } = this.state;

    const isInvalid =
      prod_code === '' ||
      prod_amt === 0 ||
      vend_to === '' ||
      vend_from === '';

    return (
      <div class="spacing">
      <h1 class="title t-spacing">Log Transaction</h1>
        <form class="form_content" onSubmit={this.onSubmit}>
          <div class="form-group">
            <input
                name="prod_code"
                value={prod_code}
                onChange={this.onChange}
                type="text"
                placeholder="Product Code"
                class="form-control"
            />
          </div>
          <div class="form-group">
          <label>Product Amount:</label>
              <input
                name="prod_amt"
                value={prod_amt}
                onChange={this.onChange}
                type="number"
                placeholder="Amount"
                class="form-control"
              />
          </div>
          <div class="form-group">
              <input
                name="vend_from"
                value={vend_from}
                onChange={this.onChange}
                type="text"
                placeholder="From"
                class="form-control"
              />
          </div>  
          <div class="form-group">
              <input
                name="vend_to"
                value={vend_to}
                onChange={this.onChange}
                type="text"
                placeholder="To"
                class="form-control"
              />
          </div>
          <button class="form_btn" disabled={isInvalid} type="submit">Log Transaction</button>
        </form>
      </div>
    );
  }
}

const LogTransactionForm = compose(
  withFirebase,
)(LogTransactionFormBase);

export default LogTransactionPage;

export { LogTransactionForm };
