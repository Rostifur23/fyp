import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";

import AddProductForm from '../AddProduct';
import AddVendorForm from '../AddVendor';
import LogTransactionForm from '../LogTransaction';
import ViewTransactions from '../ViewTransactions';



class ControlledTabs extends React.Component {
    render() {
      return (
        <Tabs>
            <TabList>
            <Tab>Add Product</Tab>
            <Tab>Add Vendor</Tab>
            <Tab>Log Transaction</Tab>
            <Tab>View Transactions</Tab>
            </TabList>

            <TabPanel>
                <AddProductForm />
            </TabPanel>
            <TabPanel>
                <AddVendorForm />
            </TabPanel>
            <TabPanel>
                <LogTransactionForm />
            </TabPanel>
            <TabPanel>
                <ViewTransactions />
            </TabPanel>
        </Tabs>
      );
    }
  }
  
export default ControlledTabs;