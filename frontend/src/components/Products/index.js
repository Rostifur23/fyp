import React, { Component } from 'react';

import { withFirebase } from '../Firebase';
import './index.css';

class ProductList extends Component {
  constructor(props) {
    super(props);

    this.myProductConnection = this.props.firebase.myProducts();

    this.state = {
      loading: false,
      products: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });

    this.myProductConnection.on('value', snapshot => {
      const productsObject = snapshot.val() || {};

      const productsList = Object.keys(productsObject).map(key => ({
        ...productsObject[key],
        pid: key,
      }));

      this.setState({
        products: productsList,
        loading: false,
      });
    });
  }

  
  componentWillUnmount() {
    this.myProductConnection.off('value');
  }

  render() {
    const { products, loading } = this.state;
    return (
      <div>
        {loading && <div>Loading ...</div>}
        
        {(!loading && !products.length) &&
          <div>
            No products found
          </div>
        }

        {(!loading && products.length > 0) &&
          <ProductsList products={products} />
        }

      </div>
    );
  }
}

const ProductsList = ({ products }) => (
<div>
  <table class="prod_table">
    <thead>
      <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Price</th>
      </tr>
		</thead>
		<tbody>
  {products.map(product => (
      <tr key={product.pid}>
        <td>{product.pid}</td>
        <td id="prod_name">{product.prod_name}</td>
        <td>€{product.prod_price}</td>
      </tr>
  ))}
    </tbody>
  </table>
</div>
);

export default withFirebase(ProductList);
