import React, { Component } from 'react';

import { withFirebase } from '../Firebase';

import './index.css';

class VendorList extends Component {
  constructor(props) {
    super(props);

    this.myVendorConnection = this.props.firebase.myVendors();

    this.state = {
      loading: false,
      vendors: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });

    this.myVendorConnection.on('value', snapshot => {
      const vendorsObject = snapshot.val() || {};

      const vendorsList = Object.keys(vendorsObject).map(key => ({
        ...vendorsObject[key],
        vid: key,
      }));

      this.setState({
        vendors: vendorsList,
        loading: false,
      });
    });
  }

  
  componentWillUnmount() {
    this.myVendorConnection.off('value');
  }

  render() {
    const { vendors, loading } = this.state;
    return (
      <div>
        {loading && <div>Loading ...</div>}
        
        {(!loading && !vendors.length) &&
          <div>
            No vendors found
          </div>
        }

        {(!loading && vendors.length > 0) &&
          <VendorsList vendors={vendors} />
        }
      </div>
    );
  }
}

const VendorsList = ({ vendors }) => (
<div>
  <table class="vend_table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Type</th>
      </tr>
    </thead>
    <tbody>
  {vendors.map(vendor => (  
    
    <tr key={vendor.vid}>
      <td>{vendor.vid}</td>
      <td id="vend_name">{vendor.vend_name}</td>
      <td id="vend_type">{vendor.vend_type}</td>
    </tr>
  ))}
    </tbody>
  </table>
</div>
);

export default withFirebase(VendorList);
