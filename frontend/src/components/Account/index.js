import React, { Component } from 'react';
import { compose } from 'recompose';

import "./index.css";
import { withFirebase } from '../Firebase';
import { PasswordForgetForm } from '../PasswordForget';
import PasswordChangeForm from '../PasswordChange';
import { withAuthorization } from '../Session';

const AccountPage = () => (
        <div class="spacing">
          <div class="border">
            <AccountDetails />
            <PasswordChangeForm />
            <PasswordForgetForm />
          </div>
        </div>
        
);

class Account extends Component {
  constructor(props) {
    super(props);
    const { uid } = props.firebase.auth.currentUser;
    this.emailRef = this.props.firebase.user(uid).child('email');
    this.compRef = this.props.firebase.user(uid).child('company_name');

    this.state = {
      uid,
      email: '',
      comp: '',
    };
  }

  componentDidMount() {
    this.emailRef.on('value', snap => {
      this.setState({ email: snap.val() });
    });

    this.compRef.on('value', snap => {
      this.setState({ comp: snap.val() });
    });
  }

  componentWillMount() {
    this.emailRef.off('value');
    this.compRef.off('value');
  }

  render () {
    const {email, comp } = this.state;
    return(
      <div class="spacing">
      <div class="">
        <h1 class="title t-spacing">Account Details</h1>
        <div class="btn_wrapper">
          <p>Email: {email}</p>
          <p>Company Name: {comp}</p>
        </div>
      </div>
      </div>
    );  
  }
}

const AccountDetails = compose(
  withFirebase,
)(Account);

const condition = authUser => !!authUser;

export default withAuthorization(condition)(AccountPage);
