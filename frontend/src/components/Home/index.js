import React from 'react';

import './index.css';
import { withAuthorization } from '../Session';
import Map from '../Map';
import ControlledTabs from '../Tabs';
import ListTabs from '../ListTabs';

const Home = () => (
  <div>
    <Map />
    <ControlledTabs />
    <hr />
    <ListTabs />
    <hr />
  </div>
);

const condition = authUser => !!authUser;

export default withAuthorization(condition)(Home);
